import MainHeader from '../system/MainHeader';
import EumHeader from '../system/EumHeader';
import EumFooter from '../system/EumFooter';

const autoOnce = () => {
  /**
   * CustomEvent polyfill.
   */
  (() => {
    if (typeof window.CustomEvent === 'function') return;

    function CustomEvent(event, params) {
      params = params || { bubbles: false, cancelable: false, detail: null };
      const evt = document.createEvent('CustomEvent');
      evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
      return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
  })();

  /**
   * Element.matches() polyfill for IE10-11
   */
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  }

  /**
   * Throttles high fire rate events with requestAnimationFrame.
   */
  (() => {
    function rAFthrottle(type, name, obj) {
      obj = obj || window;
      let running = false;
      const func = () => {
        if (running) { return; }
        running = true;
        requestAnimationFrame(() => {
          obj.dispatchEvent(new CustomEvent(name));
          running = false;
        });
      };
      obj.addEventListener(type, func);
    }

    rAFthrottle('resize', 'optimizedResize');
    rAFthrottle('scroll', 'optimizedScroll');
  })();

  const data = {};

  /**
   * System components initialization
   */
  const mainHeader = document.querySelector('.main-header');
  const eumHeader = document.querySelector('.eum-header');
  const eumFooter = document.querySelector('.eum-footer');
  if (mainHeader) {
    data.mainHeader = new MainHeader(mainHeader);
    data.mainHeader.init();
  }
  if (eumHeader) {
    data.eumHeader = new EumHeader(eumHeader);
    data.eumHeader.init();
  }
  if (eumFooter) {
    data.eumFooter = new EumFooter(eumFooter);
    data.eumFooter.init();
  }

  return data;
};

const autoAlways = () => {
  /**
   * Auto binds click on every search bar clear button
   */
  (() => {
    const buttons = document.querySelectorAll('.search button');
    buttons.forEach((el) => {
      el.addEventListener('click', () => {
        const searchBar = el.parentElement.querySelector('input');
        searchBar.value = '';
        searchBar.focus();
      });
    });
  })();

  /**
   * Auto modifies input[type=file] behavior
   */
  (() => {
    const inputs = document.querySelectorAll('input[type=file].uploader');
    inputs.forEach((el) => {
      const fileContainer = document.createElement('div');
      fileContainer.classList.add('.uploader__filelist');
      el.parentElement.appendChild(fileContainer);
      el.addEventListener('change', () => {
        const fileList = el.files;
        fileContainer.innerHTML = '';
        fileList.forEach((file) => {
          const tag = document.createElement('span');
          tag.classList.add('tag');
          tag.innerText = file.name;
          fileContainer.appendChild(tag);
        });
      });
    });
  })();
};

export { autoOnce, autoAlways };
