import gsap from 'gsap';
import Component from './_Component';

/**
 * ScrollToTop element. Built to be used in conjunction with the scrollToTop button.
 * Receives the button in the constructor, and exposes the <code>init()</code> method.
 */
class ScrollToTop extends Component {
  updateScroll() {
    window.scrollTo(0, this.scrollPosition);
  }

  setEvents() {
    this.events = this.handleEvent('optimizedScroll', {
      onElement: window,
      withCallback: () => {
        if ((window.pageYOffset > 0 && this.lastScrolled > window.pageYOffset) && !this.visible) {
          gsap.to(this.element, { duration: 0.2, display: 'block', autoAlpha: 1 });
          this.visible = true;
        }
        if ((window.pageYOffset === 0 || this.lastScrolled < window.pageYOffset) && this.visible) {
          gsap.to(this.element, { duration: 0.2, display: 'none', autoAlpha: 0 });
          this.visible = false;
        }
        this.lastScrolled = window.pageYOffset;
      },
    });

    this.events = this.handleEvent('click', {
      onElement: this.element,
      withCallback: () => {
        this.scrollPosition = window.pageYOffset;
        const duration = window.pageYOffset / 3200 < 1 ? window.pageYOffset / 3200 : 1;
        gsap.to(this, { duration, scrollPosition: 0, onUpdate: this.updateScroll.bind(this) });
      },
    });
  }

  /**
   * Initializes the ScrollToTop button
   */
  init() {
    this.element.style.display = 'none';
    this.element.style.opacity = 0;
    this.visible = false;
    this.setEvents();
  }
}

export default ScrollToTop;
