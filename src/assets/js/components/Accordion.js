/* eslint-disable max-classes-per-file */
import Component from './_Component';

/**
 * @ignore
 * Accordion subcomponent, a single collapsible. 
 * Receives the click handler as function
 */
class AccordionElement extends Component {
  constructor(element, options) {
    super(element, options);
    this.element = element;
    this.content = element.querySelector('.accordion__collapse');
    this.height = window.getComputedStyle(this.content).getPropertyValue('height');
    this.trigger = element.querySelector('.accordion__title');
    this.isOpen = false;
    // Autoinit

    this.init();
  }

  // Handles height changes
  handleResize() {
    const isClosed = this.content.style.maxHeight === '0px';
    this.content.style.maxHeight = 'none';
    this.height = window.getComputedStyle(this.content).getPropertyValue('height');
    this.content.style.maxHeight = isClosed ? 0 : this.height;
  }

  setEvents() {
    // Toggle on title click
    this.events.clickHandler = this.handleEvent('click', {
      onElement: this.trigger,
      withCallback: this.settings.handler,
    });

    // Resets height on window resize
    this.events.resizeHandler = this.handleEvent('optimizedResize', {
      onElement: window,
      withCallback: this.handleResize,
    });
  }

  open() {
    this.content.style.maxHeight = this.height;
    this.element.classList.add('is-accordion-open');
    this.isOpen = true;
  }

  close() {
    this.content.style.maxHeight = 0;
    this.element.classList.remove('is-accordion-open');
    this.isOpen = false;
  }

  // Sets events and closes the element(initial state)
  init() {
    this.setEvents();
    this.content.style.maxHeight = 0;
  }

  destroy() {
    this.unsetEvents();
    this.element.classList.remove('is-accordion-open');
    this.content.style = '';
  }
}

/**
 * Accordion component. Creates an Accordion from a well formed Accordion element. 
 * The behavior (classic accordion or collapsible) can be set via options. 
 * Exposes a <code>select()</code> method to open/close an element via JS if needed.
 */
class Accordion extends Component {
  /**
   * Gets the element and overrides default options.
   * @param {(string | HTMLElement)} element - Selector of the target element or the element itself.
   * @param {Object} options
   * @param {boolean} [options.accordion=true] - Should the element behave like an accordion?
   */
  constructor(element, options) {
    super(element, options);
    this.accordionElements = [...element.querySelectorAll('.accordion__element')];
  }

  // eslint-disable-next-line class-methods-use-this
  getDefaultOptions() {
    return {
      accordion: true,
    };
  }

  /**
   * Initializes the accordion.
   */
  init() {
    if (!this.element.classList.contains('is-accordion-enabled')) {
      this.accordions = this.accordionElements.map(
        (el, index) => new AccordionElement(el, { handler: () => this.select(index) }),
      );
      this.element.classList.add('is-accordion-enabled');
    } else {
      console.warn('JESUITS-FRAMEWORK: An Accordion is being initialized twice, there may be a problem with your code.');
    }
  }

  /**
   * Selects the nth accordion element, toggling visibility as needed.
   * @param {number} index - 0-based index of the element to select. 
   */
  select(index) {
    if (this.element.classList.contains('is-accordion-enabled')) {
      const clicked = this.accordions[index];
      if (clicked.isOpen) {
        clicked.close();
      } else {
        if (this.settings.accordion) {
          this.accordions.forEach(el => el.close());
        }
        clicked.open();
      }
    } else {
      console.warn('JESUITS-FRAMEWORK: Selecting an element of an uninitialized Accordion, there may be a problem with your code.');
    }
  }

  /**
   * Destroys the accordion.
   */
  destroy() {
    if (this.element.classList.contains('is-accordion-enabled')) {
      this.element.classList.remove('is-accordion-enabled');
      this.accordions.forEach(el => el.destroy());
      this.accordions = null;
    } else {
      console.warn('JESUITS-FRAMEWORK: An Accordion is being destroyed twice, there may be a problem with your code.');
    }
  }
}

export default Accordion;
