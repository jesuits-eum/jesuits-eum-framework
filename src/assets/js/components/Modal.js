import gsap from 'gsap';
import Component from './_Component';

/**
 * Modal Component. Displays the content passed in the <code>open()</code> method as a modal.
 * Can be closed by the user by clicking outside the modal, or by clicking 
 * on any element inside the modal itself with the <code>data-modal</code> attribute set to close.
 * Can be closed via JS with the <code>close()</code> method.
 */
class Modal extends Component {
  /**
   * Initializes the Modal manager, creates the overlay and appends it to the DOM.
   */
  constructor() {
    super();
    this.overlay = document.createElement('div');
    this.overlay.classList.add('overlay');
  }

  setEvents() {
    this.events.overlayClick = this.handleEvent('click', {
      onElement: this.overlay,
      withCallback: (e) => {
        if (e.currentTarget === e.target) this.close();
      },
    });

    this.events.escapeClose = this.handleEvent('keydown', {
      onElement: document.body,
      withCallback: (e) => {
        if (e.keyCode === 27) this.close();
      },
    });

    this.content.querySelectorAll('[data-modal=close]').forEach((el, index) => {
      this.events[`close${index}`] = this.handleEvent('click', {
        onElement: el,
        withCallback: this.close,
      });
    });
  }

  appendElements() {
    document.body.appendChild(this.overlay);
    gsap.from(this.overlay, {
      duration: 0.2,
      opacity: 0,
    });
    this.overlay.appendChild(this.content);
  }

  removeElements() {
    this.overlay.parentNode.removeChild(this.overlay);
    document.body.appendChild(this.content);
    this.content = null;
  }

  /**
   * Displays the received element as a modal.
   * @param {HTMLElement} element - The element to display, has to be a div with class <code>modal</code>.
   */
  open(element) {
    this.content = element;
    this.setEvents();
    this.appendElements();

    // Lock screen
    document.body.style.top = `-${window.scrollY}px`;
    document.body.style.position = 'fixed';
    document.body.classList.add('has-modal-open');
  }

  /**
   * Closes the Modal.
   */
  close() {
    this.unsetEvents();
    this.removeElements();

    // Unlock screen
    const { top } = document.body.style;
    document.body.style.position = '';
    document.body.style.top = '';
    window.scrollTo(0, parseInt(top || '0', 10) * -1);
    document.body.classList.remove('has-modal-open');
  }
}

export default Modal;
