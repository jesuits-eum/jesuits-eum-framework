import flatpickr from 'flatpickr';
import { Italian } from 'flatpickr/dist/l10n/it';
import Component from './_Component';

/**
 *  DateInput component, mostly a wrapper for [flatpickr.js]{@link https://flatpickr.js.org/}
 *  so you can pass almost any option from {@link https://flatpickr.js.org/options/},
 *  with an exception being locale: currently only Italian is supported with <code>locale: 'it'</code>.
 */
class DateInput extends Component {
  /**
   * Autoinizializes the DateInput.
   * @param {(string | HTMLElement)} element - Selector of the target element or the element itself. Must be an <code>input</code> of <code>type="date"<code/>
   * @param {Object} options - Any option from {@link https://flatpickr.js.org/options/} except locale, see below
   * @param {string} [options.locale=en] - Language code, either <code>it</code> or <code>en</code>
   */
  constructor(element, options) {
    super(element, options);
    this.getLocale();
    this.instance = flatpickr(element, this.settings);
  }

  // eslint-disable-next-line class-methods-use-this
  getDefaultOptions() {
    return {
      dateFormat: 'd/m/Y',
      locale: 'en',
    };
  }

  getLocale() {
    if (this.settings.locale === 'it') this.settings.locale = Italian;
  }

  /**
   * Destroys the DateInput.
   */
  destroy() {
    this.instance.destroy();
  }
}

export default DateInput;
