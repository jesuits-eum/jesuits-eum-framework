import Component from './_Component';

/**
 * Tab Component. Generates a classic click-based Tab system from a well formed Tab element, 
 * exposes a <code>selectPanel()</code> method to control the tabbing via JS if needed.
 */
class Tabs extends Component {
  /**
   * Receives the Tab element and checks its structure, eventually logs a warning.
   * @param {(string | HTMLElement)} element - Selector of the Tab element or the element itself. 
   */
  constructor(element) {
    super(element);
    this.controls = element.querySelectorAll('.tabs__control');
    this.panels = element.querySelectorAll('.tabs__panel');

    if (this.panels.length !== this.controls.length) {
      console.warn('JESUITS - FRAMEWORK: A Tabs component was created with more/less controls than panels');
    }
  }

  /**
   * Hides the last active panel and displays the supplied one.
   * @param {number} n - The panel to display, 0-based
   */
  selectPanel(n) {
    const current = this.selected || 0;

    this.controls[current].classList.remove('is-tab-selected');
    this.panels[current].style.display = 'none';

    this.controls[n].classList.add('is-tab-selected');
    this.panels[n].style.display = 'block';

    this.selected = n;
  }

  setEvents() {
    this.controls.forEach((control, index) => {
      this.events[`control${index}`] = this.handleEvent('click', {
        onElement: control,
        withCallback: (e) => {
          const n = Array.prototype.indexOf.call(this.controls, e.currentTarget);
          this.selectPanel(n);
        },
      });
    });
  }

  /**
   * Initalizes the Tab component.
   */
  init() {
    this.setEvents();
    this.panels.forEach((panel) => { panel.style.display = 'none'; });
    this.selectPanel(0);
  }

  /**
   * Destroys the Tab component.
   */
  destroy() {
    this.unsetEvents();

    this.panels.forEach((panel) => { panel.style.display = 'block'; });

    const n = this.selected;
    this.controls[n].classList.remove('is-tab-selected');
  }
}

export default Tabs;
