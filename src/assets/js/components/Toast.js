import gsap from 'gsap';
import Draggable from 'gsap/Draggable';
import Component from './_Component';

/**
 * Toast component. Creates a Toast ready to be dispatched via its <code>dispatch()</code> method. 
 * The Toast automatically removes itself from the DOM after the supplied duration, 
 * but can be removed early if needed by calling its <code>remove()</code> method.
 */
class Toast extends Component {
  /**
   * Receives the options, checks if they're valid and creates a new Toast Object.
   * @param {Object} options
   * @param {string} options.message - The message of the Toast is the only required parameter.
   * @param {string} [options.title] - The title of the Toast.
   * @param {string} [options.type=info] - The type of the Toast: success, warning, info or error.
   * @param {number} [options.duration=4] - The duration in seconds of the Toast.
   */
  constructor(options) {
    if (!Object.prototype.hasOwnProperty.call(options, 'message') || typeof options.message !== 'string') {
      console.warn('JESUITS-FRAMEWORK: A toast was initialized incorrectly with an invalid message, or without a message');
      return null;
    }
    super(null, options);
    this.element = null;
    gsap.registerPlugin(Draggable);
  }

  // eslint-disable-next-line class-methods-use-this
  getDefaultOptions() {
    return {
      title: '',
      type: 'info',
      duration: 4,
      // position: 'right-bottom',
    };
  }

  buildToast() {
    const {
      message,
      title,
      type,
      // position,
    } = this.settings;
    const container = document.createElement('div');
    container.classList.add('toast');
    switch (type) {
      case 'info':
        container.classList.add('toast--info');
        break;
      case 'warning':
        container.classList.add('toast--warning');
        break;
      case 'error':
        container.classList.add('toast--error');
        break;
      case 'success':
        container.classList.add('toast--success');
        break;
      default:
        container.classList.add('toast--info');
        // TODO: move all checks and warnings in the constructor
        console.warn('JESUITS-FRAMEWORK: Invalid toast type, defaulted to \'info\'.');
    }

    if (title !== '') {
      const titleElement = document.createElement('p');
      titleElement.classList.add('toast__title');
      titleElement.innerText = title;
      container.appendChild(titleElement);
    }

    const messageElement = document.createElement('p');
    messageElement.classList.add('toast__message');
    messageElement.innerText = message;
    container.appendChild(messageElement);

    return container;
  }

  /**
   * Hides the Toast, eventually animating it.
   * @param {(boolean | string)} [animated=false] - Has to be animated?
   */
  remove(animated = false) {
    if (animated) {
      gsap.to(this.element, {
        duration: 0.2,
        opacity: 0,
        x: 100,
        onComplete: () => {
          this.element.parentNode.removeChild(this.element);
          this.element = null;
        },
      });
    } else {
      window.clearTimeout(this.timer);
      this.element.parentNode.removeChild(this.element);
      this.element = null;
    }
  }

  initDraggable() {
    const removeEl = this.remove.bind(this);
    // 'this' refers to the draggable, not to the Toast
    function onDrag() {
      const opacity = 1 - (this.x / this.maxX);
      gsap.set(this.target, {
        opacity,
      });
    }

    // 'this' refers to the draggable, not to the Toast
    function onDragEnd() {
      if (this.endX < this.maxX / 2) {
        gsap.to(this.target, {
          duration: 0.2,
          opacity: 1,
          x: this.minX,
        });
      } else {
        gsap.to(this.target, {
          duration: 0.2,
          opacity: 0,
          x: this.maxX,
          onComplete: removeEl,
        });
      }
    }

    this.draggable = Draggable.create(this.element, {
      type: 'x',
      bounds: { minX: 0, maxX: 100 },
      onDragEnd,
      onDrag,
    });
    // Draggable.create returns an array, so we "pop" out the instance
    this.draggable = this.draggable.pop();
  }

  /**
   * Dispatches the Toast.
   */
  dispatch() {
    let container = document.querySelector('.toast-container');
    // If element is already dispatched, reset its timer
    if (this.element) {
      window.clearTimeout(this.timer);
      this.timer = window.setTimeout(() => {
        if (this.element) {
          this.draggable.disable();
          this.remove('animated');
        }
      }, this.settings.duration * 1000);
      return;
    }

    // Build the element
    this.element = this.buildToast();
    this.element.style.opacity = 0;

    // Insert it into the DOM with a FadeIn
    if (!container) {
      container = document.createElement('div');
      container.classList.add('toast-container');
      document.body.appendChild(container);
    }
    container.appendChild(this.element);
    gsap.fromTo(this.element, {
      duration: 0.2,
      opacity: 0,
      x: 100,
    }, {
      opacity: 1,
      x: 0,
    });

    // Make it draggable
    this.initDraggable();

    // If it waasn't swiped out,
    // remove it after the specified duration
    this.timer = window.setTimeout(() => {
      if (this.element) {
        this.draggable.disable();
        this.remove('animated');
      }
    }, this.settings.duration * 1000);
  }
}

export default Toast;
