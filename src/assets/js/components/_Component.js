/* eslint-disable class-methods-use-this */
/**
 * Base Component Class
 */

export default class Component {
  /**
   * Gets the element and overrides default options.
   * @param {[string | HTMLElement]} element - Selector of the target element or the element itself
   * @param {Object} options - Options overriding defaults
   */
  constructor(element, options) {
    this.element = typeof element === 'string'
      ? document.querySelector(element)
      : element;
    this.settings = { ...this.getDefaultOptions(), ...options };
    this.events = {};
  }

  /**
   * Functions returning the default options.
   * Should replaced with the default for the extended component
   * @returns {Object} Default options
   */
  getDefaultOptions() {
    return {
      // Default options
    };
  }

  /**
   * Add a listener to a given element for a given event,
   * that will execute the given function with the given context.
   * Returns an handler to remove the listener.
   * @param {string} eventName - Event
   * @param {Object} param1
   * @param {HTMLElement} param1.onElement - Element to bind the listener to
   * @param {Function} param1.withCallback - Function that will be called
   * @param {boolean} [param1.useCapture=false] - Optional useCapture parameter for addEventListener
   * @param {*} [thisArg=this] - Optional context for the called function
   * @returns {Function} Event handler with the destroy() method.
   */
  handleEvent(eventName, { onElement, withCallback, useCapture = false } = {}, thisArg = this) {
    const element = onElement || document.documentElement;

    function handler(event) {
      if (typeof withCallback === 'function') {
        withCallback.call(thisArg, event);
      }
    }

    handler.destroy = () => element.removeEventListener(eventName, handler, useCapture);

    element.addEventListener(eventName, handler, useCapture);

    return handler;
  }

  /**
   * Destroy all the stored events
   */
  unsetEvents() {
    Object.keys(this.events).forEach((key) => {
      this.events[key].destroy();
    });
  }
}
