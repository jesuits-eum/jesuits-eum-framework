import Component from './_Component';

/**
 * Progress Component. Exposes <code>prev()</code> and <code>next()</code> methods to control a progress element. 
 * Detects the initial state from the element with class <code>progress__step--current</code>.
 */
class Progress extends Component {
  /**
   * Initializes the Progress on the received element. 
   * Infers the initial state from the element structure and 
   * eventually cleans it up.
   * @param {HTMLElement} element - The progress element
   */
  constructor(element) {
    super(element);

    this.steps = element.children;
    // Get initial state from the element
    if (element.querySelector('.progress__step--current')) {
      this.current = Array.prototype.indexOf.call(this.steps, element.querySelector('.progress__step--current'));
    } else {
      this.current = 0;
    }

    // Reset elements
    const childrens = [...this.steps];
    childrens.forEach((el, index) => {
      if (index < this.current) {
        el.className = 'progress__step progress__step--done';
      } else if (index > this.current) {
        el.className = 'progress__step';
      } else {
        el.classList.add('progress__step--current');
      }
    });
  }

  /**
   * Increment the Progress.
   */
  next() {
    const curr = this.current;
    const next = curr + 1;

    if (curr >= this.steps.length) return;

    this.steps[curr].classList.remove('progress__step--current');
    this.steps[curr].classList.add('progress__step--done');
    // If not completed, modify the next step.
    if (next < this.steps.length) this.steps[next].classList.add('progress__step--current');
    this.current = next;
  }

  /**
   * Decrement the Progress.
   */
  prev() {
    const curr = this.current;
    const prev = curr - 1;

    if (curr <= 0) return;

    this.steps[prev].classList.remove('progress__step--done');
    this.steps[prev].classList.add('progress__step--current');
    // If not completed, modify the current step.
    if (curr < this.steps.length) this.steps[curr].classList.remove('progress__step--current');
    this.current = prev;
  }
}

export default Progress;
