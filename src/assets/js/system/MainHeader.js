/* eslint-disable class-methods-use-this */
import Component from '../components/_Component';
import Dropdown from './_Dropdown';
import StickyHeader from './_StickyHeader';

class MainHeader extends Component {
  constructor(element, settings) {
    super(element, settings);

    this.header = this.element;
    this.state = {
      initialized: false,
      menuInitialized: false,
    };
  }

  getDefaultOptions() {
    return {
      hasStickyHeader: false,
    };
  }

  init() {
    const { header } = this;
    // Check required HTML
    if (!header) {
      console.warn('JESUITS-FRAMEWORK: No main-header found');
      return;
    }
    this.state.initialized = true;

    // Initialize Menu
    this.mainMenuTrigger = header.querySelector('.js-main-header-trigger');
    this.mainMenu = header.querySelector('.main-header__mobile-wrapper');
    if (this.mainMenu && this.mainMenuTrigger) {
      this.state.menuInitialized = true;
      this.events.hamburgerClick = this.handleEvent('click', {
        onElement: this.mainMenuTrigger,
        withCallback: this.toggleMenu,
      });
    } else {
      console.warn('JESUITS-FRAMEWORK: HTML elements not found, menu not initialized');
    }

    // Initialize Menu Dropdowns
    const headerDropdowns = header.querySelectorAll('.js-dropdown');
    headerDropdowns.forEach(el => new Dropdown(el).init());

    // Initialize sticky Header
    if (this.settings.hasStickyHeader || header.classList.contains('js-header-sticky')) {
      this.stickyHeader = new StickyHeader(header);
      this.stickyHeader.init();
    }
  }

  closeMenu() {
    if (this.state.initialized && this.state.menuInitialized) {
      this.header.classList.remove('is-header-open');
      this.mainMenuTrigger.setAttribute('aria-expanded', 'false');
    } else {
      console.warn('JESUITS-FRAMEWORK: Trying to close an uninitialized menu');
    }
  }

  openMenu() {
    if (this.state.initialized && this.state.menuInitialized) {
      this.header.classList.add('is-header-open');
      this.mainMenuTrigger.setAttribute('aria-expanded', 'true');
    } else {
      console.warn('JESUITS-FRAMEWORK: Trying to open an uninitialized menu');
    }
  }

  toggleMenu() {
    return this.header.classList.contains('is-header-open') ? this.closeMenu() : this.openMenu();
  }

  destroy() {
    if (this.state.initialized) {
      this.unsetEvents();
      if (this.settings.hasStickyHeader) this.stickyHeader.destroy();
    } else {
      console.warn('JESUITS-FRAMEWORK: Trying to destroy an uninitialized main-header');
    }
  }
}

export default MainHeader;
