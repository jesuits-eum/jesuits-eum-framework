import Component from '../components/_Component';

class EumFooter extends Component {
  init() {
    const footer = this.element;
    // Check required HTML
    if (!footer) {
      console.warn('JESUITS-FRAMEWORK: No eum-footer found');
      return;
    }

    const footerTrigger = footer.querySelector('.js-main-footer-trigger');
    this.events.footerDropdown = this.handleEvent('click', {
      onElement: footerTrigger,
      withCallback: () => {
        if (footer.classList.contains('is-footer-open')) {
          footer.classList.remove('is-footer-open');
          footerTrigger.setAttribute('aria-expanded', 'false');
        } else {
          footer.classList.add('is-footer-open');
          footerTrigger.setAttribute('aria-expanded', 'true');
        }
      },
    });
  }
}

export default EumFooter;
