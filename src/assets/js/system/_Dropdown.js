import Component from '../components/_Component';

class Dropdown extends Component {
  constructor(element) {
    super(element);
    this.trigger = this.element.querySelector('.js-dropdown-trigger');
    this.dropdownContainer = this.element.querySelector('.js-dropdown-content');

    this.eventTypes = window.PointerEvent ? {
      end: 'pointerup',
      enter: 'pointerenter',
      leave: 'pointerleave',
    } : {
      end: window.MSPointerEvent ? 'click' : 'touchend',
      enter: 'mouseenter',
      leave: 'mouseleave',
    };
  }

  init() {
    this.setEvents();
  }

  toggleDropdown() {
    return this.active === true ? this.closeDropdown() : this.openDropdown();
  }

  startCloseTimeout() {
    this.closeDropdownTimeout = setTimeout(() => {
      this.closeDropdown();
    }, 50);
  }

  stopCloseTimeout() {
    clearTimeout(this.closeDropdownTimeout);
  }

  closeDropdown() {
    this.element.classList.remove('is-dropdown-open');
    this.trigger.setAttribute('aria-expanded', 'false');
    this.active = false;
  }

  openDropdown() {
    this.element.classList.add('is-dropdown-open');
    this.trigger.setAttribute('aria-expanded', 'true');
    this.active = true;
  }

  setEvents() {
    const m = this.eventTypes;

    this.events.triggerClickHandler = this.handleEvent(m.end, {
      onElement: this.trigger,
      withCallback: (evt) => {
        evt.preventDefault();
        evt.stopPropagation();
        this.toggleDropdown();
      },
    });

    this.events.triggerHoverInHandler = this.handleEvent(m.enter, {
      onElement: this.trigger,
      withCallback: (evt) => {
        if (evt.pointerType === 'touch') { return; }
        this.stopCloseTimeout();
        this.openDropdown();
      },
    });

    this.events.triggerHoverOutHandler = this.handleEvent(m.leave, {
      onElement: this.trigger,
      withCallback: (evt) => {
        if (evt.pointerType === 'touch') { return; }
        this.startCloseTimeout();
      },
    });

    this.events.internalClickHandler = this.handleEvent(m.end, {
      onElement: this.dropdownContainer,
      withCallback: evt => evt.stopPropagation(),
    });

    this.events.internalHoverInHandler = this.handleEvent(m.enter, {
      onElement: this.dropdownContainer,
      withCallback: (evt) => {
        if (evt.pointerType === 'touch') { return; }
        this.stopCloseTimeout();
      },
    });

    this.events.internalHoverOutHandler = this.handleEvent(m.leave, {
      onElement: this.dropdownContainer,
      withCallback: (evt) => {
        if (evt.pointerType === 'touch') { return; }
        this.startCloseTimeout();
      },
    });

    this.events.externalClickHandler = this.handleEvent(m.end, {
      onElement: window,
      withCallback: () => {
        if (!window.matchMedia('(max-width: 1199px)').matches) this.closeDropdown();
      },
    });

    this.events.focusHandler = this.handleEvent('focusin', {
      onElement: window,
      withCallback: (evt) => {
        if (evt.target === this.trigger) {
          this.stopCloseTimeout();
          this.openDropdown();
        } else if (!this.dropdownContainer.contains(evt.target)) {
          this.startCloseTimeout();
        }
      },
    });
  }
}

export default Dropdown;
