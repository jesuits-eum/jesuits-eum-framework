import Component from '../components/_Component';
import Dropdown from './_Dropdown';

class EumHeader extends Component {
  init() {
    const eumHeader = this.element;
    // Check required HTML
    if (!eumHeader) {
      console.warn('JESUITS-FRAMEWORK: No eum-header found');
      return;
    }
    // Initialize Menu Dropdowns
    const headerDropdowns = eumHeader.querySelectorAll('.js-dropdown');
    headerDropdowns.forEach(el => new Dropdown(el).init());
  }
}

export default EumHeader;
