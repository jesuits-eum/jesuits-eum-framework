/* eslint-disable class-methods-use-this */
import Component from '../components/_Component';

export default class StickyHeader extends Component {
  init() {
    // Creates a placeholder to avoid layout jumps when the header gets stickied
    const header = this.element;
    this.placeholder = document.createElement('div');
    this.oldScroll = window.pageYOffset;
    const { placeholder } = this;
    placeholder.classList.add('main-header__placeholder');
    header.parentNode.insertBefore(placeholder, header);
    placeholder.style.height = `${header.getBoundingClientRect().height}px`;
    header.parentNode.insertBefore(placeholder, header.nextSibling);
    placeholder.appendChild(header);

    // Sticks header when the topside of the placeholder is above the viewport
    this.headerY = window.pageYOffset + placeholder.getBoundingClientRect().top;

    this.events.onScroll = this.handleEvent('optimizedScroll', {
      onElement: window,
      withCallback: () => {
        if (window.pageYOffset >= this.headerY) {
          document.body.classList.add('has-header-sticky');
        } else {
          // Prevent unsticking on modal open
          if (document.body.matches('.has-header-sticky.has-modal-open')) return;
          document.body.classList.remove('has-header-sticky');
        }

        if (window.pageYOffset <= this.oldScroll) {
          document.body.classList.add('has-scrolled-down');
        } else {
          document.body.classList.remove('has-scrolled-down');
        }

        this.oldScroll = window.pageYOffset;
      },
    });

    // Size recalculation on resize
    this.events.onResize = this.handleEvent('optimizedResize', {
      onElement: window,
      withCallback: this.measure,
    });

    // First measurement
    this.measure();
  }

  checkMedia() {
    if (window.matchMedia('(min-width: 1599px)').matches) {
      return 'xxl';
    }

    if (window.matchMedia('(min-width: 1199px)').matches) {
      return 'xl';
    }

    if (window.matchMedia('(min-width: 564px)').matches) {
      return 'm';
    }

    return 'xs';
  }

  measure() {
    const header = this.element;
    const headerWrapper = header.querySelector('.main-header__wrapper');
    const headerImage = header.querySelector('.main-header__brand-logo');
    // Check if recalculation is needed
    const currentStatus = this.checkMedia();
    if (currentStatus === this.currentStatus) return;
    this.currentStatus = currentStatus;
    // Disable transitions
    header.classList.add('force-no-transition');
    if (headerImage) {
      headerImage.classList.add('force-no-transition');
    }
    if (headerWrapper) {
      headerWrapper.classList.add('force-no-transition');
    }

    // Eventually unstick
    document.body.classList.remove('has-header-sticky');

    // Trigger reflow, see https://stackoverflow.com/a/16575811
    // eslint-disable-next-line no-unused-expressions
    header.offsetHeight;

    // Recalc sizes
    this.placeholder.style.height = `${header.getBoundingClientRect().height}px`;
    this.headerY = window.pageYOffset + this.placeholder.getBoundingClientRect().top;

    // Eventually restick
    if (window.pageYOffset >= this.headerY) {
      document.body.classList.add('has-header-sticky');
    } else {
      document.body.classList.remove('has-header-sticky');
    }
    // Renable transitions
    header.classList.remove('force-no-transition');
    if (headerImage) {
      headerImage.classList.remove('force-no-transition');
    }
    if (headerWrapper) {
      headerWrapper.classList.remove('force-no-transition');
    }
  }

  destroy() {
    this.unsetEvents();
    this.placeholder.parentNode.insertBefore(this.element, this.placeholder);
    this.placeholder.parentNode.removeChild(this.placeholder);
  }
}
