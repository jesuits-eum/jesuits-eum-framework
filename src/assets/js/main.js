import 'ie11-custom-properties';

import { autoOnce, autoAlways } from './utils/plugins';

import Accordion from './components/Accordion';
import Toast from './components/Toast';
import Tabs from './components/Tabs';
import Progress from './components/Progress';
import Modal from './components/Modal';
import ScrollToTop from './components/ScrollToTop';
import DateInput from './components/DateInput';
import MainHeader from './system/MainHeader';
import EumHeader from './system/EumHeader';
import EumFooter from './system/EumFooter';

const data = autoOnce();
autoAlways();

export {
  Accordion,
  Toast,
  Tabs,
  Progress,
  Modal,
  ScrollToTop,
  DateInput,
  autoAlways,
  MainHeader,
  EumHeader,
  EumFooter,
  data,
};
