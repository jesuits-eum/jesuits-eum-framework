/**
 * Use this file to test development code.
 * It will NOT be included in the main bundle
 */
console.log('Testing...');
var accordion = new jesuitsFramework.Accordion(document.querySelector('#accordion'));
accordion.init();
var collapsible = new jesuitsFramework.Accordion(document.querySelector('#collapsible'), {
  accordion: false
});
collapsible.init();
document.getElementById('destroy-test').addEventListener('click', function () {
  accordion.destroy();
});
document.getElementById('reinit-test').addEventListener('click', function () {
  accordion.init();
});
var toastTestInfo = new jesuitsFramework.Toast({
  message: 'This an info toast, can be swiped out',
  title: 'Toast Example'
});
var toastTestError = new jesuitsFramework.Toast({
  message: 'This an error toast, can be swiped out',
  title: 'Something went wrong',
  type: 'error'
});
document.getElementById('toast-info-test').addEventListener('click', function () {
  toastTestInfo.dispatch();
});
document.getElementById('toast-error-test').addEventListener('click', function () {
  toastTestError.dispatch();
});
document.getElementById('toast-success-test').addEventListener('click', function () {
  new jesuitsFramework.Toast({
    message: 'This toast was created & dispatched on click',
    title: 'Success!',
    type: 'success'
  }).dispatch();
});
var tabsTest = new jesuitsFramework.Tabs(document.querySelector('.tabs'));
tabsTest.init();
tabsTest.selectPanel(1);
var progTest = new jesuitsFramework.Progress(document.querySelector('.progress'));
var counter = document.getElementById('step-counter-test');
counter.innerHTML = progTest.current;
document.getElementById('prev-test').addEventListener('click', function () {
  progTest.prev();
  counter.innerHTML = progTest.current;
});
document.getElementById('next-test').addEventListener('click', function () {
  progTest.next();
  counter.innerHTML = progTest.current === progTest.steps.length ? 'done' : progTest.current;
});
var modalManager = new jesuitsFramework.Modal();
document.getElementById('modal').addEventListener('click', function () {
  modalManager.open(document.querySelector('.modal'));
});
var scrollToTopTest = new jesuitsFramework.ScrollToTop(document.getElementById('scroll-to-top'));
scrollToTopTest.init();

var dateInput = new jesuitsFramework.DateInput(document.getElementById('test-inputdate'));

var dateInputLocale = new jesuitsFramework.DateInput(document.getElementById('test-inputdate-locale'), {
  locale: 'it'
});

var dateInput2 = new jesuitsFramework.DateInput(document.getElementById('test-2-inputdate'));

var dateInputLocale2 = new jesuitsFramework.DateInput(document.getElementById('test-2-inputdate-locale'), {
  locale: 'it'
});

// End test code