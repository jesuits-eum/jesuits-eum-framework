import gulp from 'gulp';
import log from 'fancy-log';

import config from './config';
import { scripts } from './scripts';
import { server } from './server';
import { scss } from './scss';
import { build } from './build';

const watch = gulp.series(
  gulp.parallel(scss, scripts),
  server,
);

// Debugger task, log anything
gulp.task('log', (done) => {
  log(config.src === config.build);
  done();
});

export {
  scss,
  scripts,
  server,
  build,
};

/* DEFAULT */
export default watch;
