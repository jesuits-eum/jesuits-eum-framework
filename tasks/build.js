import gulp from 'gulp';
import del from 'del';

import config from './config';
import { scripts } from './scripts';
import { scss } from './scss';

// Minitask to clean the build folder (Avoid deleting base folder PLS)
const clean = () => (
  config.build !== './'
    ? del(`${config.build}/**/*`)
    : null
);

// Files to move TODO: move to config;
const filesToMove = [
  `${config.src}/assets/css/main.css`,
  `${config.src}/assets/font/*.*`,
  `${config.src}/**/*.php`,
  `${config.src}/**/*.html`,
  `${config.src}/*`,
];

// Minitask to move files to the build folder
const relocate = (done) => {
  gulp.src(filesToMove, { base: config.src })
    .pipe(gulp.dest(`${config.build}/`));
  done();
};

// Clean the build folder, optimize assets, move everything in the build folder
const build = config.src !== config.build
  ? gulp.series(
    clean,
    gulp.parallel(scripts, scss),
    relocate,
  )
  : gulp.parallel(scripts, scss);

export { build };
