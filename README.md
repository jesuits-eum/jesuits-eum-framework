# Jesuits Framework pre-alpha
Development repository for the Jesuits UI Kit

## Requirements
- Yarn

## Installation
1. Clone this repo
2. Open your terminal in the cloned folder
3. Install dependencies with `yarn install`

## Usage
- `yarn start` to boot up the development server
- `yarn build` to compile the production files in the `build/` folder

## Code linting
On VsCode, install [Stylelint](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint) for CSS and SCSS linting and [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) for JS linting.

The other required files and configuration are already included in this repo, feel free to change the configs.
___

![Gidio!](https://5adesign.it/assets/img/gidio.gif "Gidio!") That's all folks, happy coding!